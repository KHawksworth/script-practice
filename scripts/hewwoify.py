'''this script will convert strings passed as script args into hewwo speak'''

import argparse

parser = argparse.ArgumentParser(description='convert to hewwo speak')
parser.add_argument('sentence', metavar='words', nargs="+", help='a sentence to be converted to hewwo speak')

hewwo_pattern = {
    "e":"3",
    "o":"0",
    "l":"w",
    "r":"w",
    "m":"mw",
    "you":"chu",
    "that":"dat",
    }

sentence = ' '.join(parser.parse_args().sentence)

hewwo_arr = []

for i in range(len(sentence)):
    if sentence[i] in list(hewwo_pattern.keys()): 
        hewwo_arr.append(hewwo_pattern.get(sentence[i]))
    else:
        hewwo_arr.append(sentence[i])
hewwo_sentence = ''.join(hewwo_arr)
print(hewwo_sentence)
