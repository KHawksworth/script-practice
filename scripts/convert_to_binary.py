import argparse

parser = argparse.ArgumentParser(description='convert to binary')
parser.add_argument('integers', metavar='number', type=int, help='a number to be converted to binary', choices=range(1, 18446744073709551615))

args = parser.parse_args()
number = args.integers
binary = bin(number)[2:]
print(binary)